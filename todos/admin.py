from django.contrib import admin
from todos.models import TodoList, TodoItem

# Register your models here.

# This is so we can see our Todo List in our /admin page
@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
    ]

# This is so we can see the Todo items for our list
@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = [
        "task",
        "due_date",
        "is_completed",
        "list",
        "id",
    ]
