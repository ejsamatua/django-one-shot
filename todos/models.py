from django.db import models

# Create your models here.

# creating the ONE part of the one-to-many relationship


class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

# creating the MANY part of the one-to-MANY relationship


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(null=True, blank=True,)
    is_completed = models.BooleanField(default=False)

    # this foreign key is connecting what items of the Todo list we need to do
    # the list is one and the items can be many of what we need to complete.
    list = models.ForeignKey(
        "TodoList",
        related_name="items",
        on_delete=models.CASCADE,
    )
