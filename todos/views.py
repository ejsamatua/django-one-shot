from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoListitemForm


# Create your views here.

######################################################################
# this let's us see what our Todolist is entering localhost:8000/todos
# and the number of items in each of that list we need to do
def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }

    return render(request, "todos/list.html", context)

######################################################################
# this will let us click on a 'todo' name and let us see what items
# we need to do.


def show_todo(request, id):
    todos = TodoList.objects.get(id=id)

    context = {
        "todo_object": todos,
    }

    return render(request, "todos/detail.html", context)

######################################################################
# this will allow us to add to our to do list


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("show_todo", id=list.id)

    else:
        form = TodoListForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)

######################################################################
# this will allow us to edit a name of our to do list


def todo_list_update(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("show_todo", id=list.id)
    else:
        form = TodoListForm(instance=list)

    context = {
        "form": form
    }

    return render(request, "todos/edit.html", context)

######################################################################
# this will allow us to delete a name of our to do list and the tasks


def todo_list_delete(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list")
    return render(request, "todos/delete.html")

######################################################################
# this will allow us to add tasks to our to do list item


def todo_item_create(request):
    if request.method == "POST":
        form = TodoListitemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("show_todo", id=item.list.id)

    else:
        form = TodoListitemForm()

    context = {
        "form": form
    }

    return render(request, "todos/create.html", context)

######################################################################
# this will allow us to add tasks to our to do list item


def todo_item_update(request, id):
    list = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoListitemForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("show_todo", id=list.id)
    else:
        form = TodoListitemForm(instance=list)

    context = {
        "form": form
    }

    return render(request, "todos/edit.html", context)
